# Building the TFM

To get started with building the TFM refer to the local dev instructions: [Readme.md](../../deploy/local/README.md).

The steps to build a docker image for use in prod are the same as for local dev, which is why we are re-using the local dev Readme.