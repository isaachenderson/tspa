[Local Build](#how-to-build-the-project-locally)
# How to Build the project Locally
```bash
    git clone --recurse-submodules https://gitlab.eclipse.org/eclipse/xfsc/train/tspa.git
    cd tspa/
    mvn clean install
    docker-compose -f docker-compose.yml pull
    docker-compose up -d --build # Build images before starting containers. 

```

On successful build using the above commands will start TSPA service on your localhost at port `16003`, to check service status open its health page in browser at `http://localhost:16003/tspa-service/actuator/health`'.

[Docker Run](#how-to-run-the-docker-image-published-on-harbour-registry)
# How to run the docker image published on Harbour Registry

```bash
    git clone https://gitlab.eclipse.org/eclipse/xfsc/train/tspa.git
    cd tspa/deploy/local
    docker-compose up -d --pull always"|"missing"|"never"
```
